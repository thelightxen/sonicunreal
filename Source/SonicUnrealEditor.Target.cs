// (c) TheLightXen, Sonic Team, Sega.

using UnrealBuildTool;
using System.Collections.Generic;

public class SonicUnrealEditorTarget : TargetRules
{
	public SonicUnrealEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "SonicUnreal" } );
	}
}
