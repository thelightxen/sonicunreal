// (c) TheLightXen, Sonic Team, Sega.

#include "SonicUnreal.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SonicUnreal, "SonicUnreal" );
