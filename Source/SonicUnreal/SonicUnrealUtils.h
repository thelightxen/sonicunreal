// (c) TheLightXen, Sonic Team, Sega.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SonicUnrealUtils.generated.h"

/**
 * 
 */
UCLASS()
class SONICUNREAL_API USonicUnrealUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, category="Game|Viewport", meta=(DefaultToSelf = "WorldContextObject", HidePin = "WorldContextObject"))
		static void SetWindowPosition(UObject* WorldContextObject, FVector2D WindowPosition);

	UFUNCTION(BlueprintPure, category = "User Information", meta = (DefaultToSelf = "WorldContextObject", HidePin = "WorldContextObject"))
		static const FVector2D GetMonitorResolution(UObject* WorldContextObject);

	UFUNCTION(BlueprintPure, category = "Game|Viewport", meta = (DefaultToSelf = "WorldContextObject", HidePin = "WorldContextObject"))
		static const FVector2D GetCenterOfViewportOnMonitor(UObject* WorldContextObject);
};
