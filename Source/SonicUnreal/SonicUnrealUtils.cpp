// (c) TheLightXen, Sonic Team, Sega.


#include "SonicUnrealUtils.h"
#include "GenericPlatform/GenericApplication.h"
#include "GameFramework/GameUserSettings.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

void USonicUnrealUtils::SetWindowPosition(UObject* WorldContextObject, FVector2D WindowPosition)
{

#if !WITH_EDITOR

    if (GEngine && GEngine->GameViewport)
    {

        GEngine->GameViewport->GetWindow()->MoveWindowTo(WindowPosition);

    }

#endif

}

const FVector2D USonicUnrealUtils::GetMonitorResolution(UObject* WorldContextObject)
{
    
    return FVector2D(float(GetSystemMetrics(SM_CXSCREEN)), float(GetSystemMetrics(SM_CYSCREEN)));

}

const FVector2D USonicUnrealUtils::GetCenterOfViewportOnMonitor(UObject* WorldContextObject)
{

    return FVector2D(GetMonitorResolution(WorldContextObject).X / 2 - UGameUserSettings::GetGameUserSettings()->GetScreenResolution().X * 0.5, GetMonitorResolution(WorldContextObject).Y / 2 - UGameUserSettings::GetGameUserSettings()->GetScreenResolution().Y * 0.5);

}