// (c) TheLightXen, Sonic Team, Sega.

using UnrealBuildTool;
using System.Collections.Generic;

public class SonicUnrealTarget : TargetRules
{
	public SonicUnrealTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "SonicUnreal" } );
	}
}
